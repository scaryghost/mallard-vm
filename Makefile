VOLAUVENT_HOME=buildSrc/volauvent
CXXFLAGS := -std=c++14

.PHONY: all clean

all: src-all
clean: src-clean

include $(VOLAUVENT_HOME)/global_config.mk

include src/rules.mk
