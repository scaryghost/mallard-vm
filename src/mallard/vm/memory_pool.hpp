#pragma once

#include <cstddef>

namespace mallard {
namespace vm {

struct MemoryPool {
    virtual ~MemoryPool() = 0;

    virtual void* allocate(std::size_t n_bytes) = 0;
    virtual void free(void* addr) = 0;
};

}
}