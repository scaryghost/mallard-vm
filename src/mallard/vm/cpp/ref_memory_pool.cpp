#include "ref_memory_pool.hpp"

#include <cstdlib>

using std::free;
using std::malloc;
using std::size_t;

namespace mallard {
namespace vm {

RefMemoryPool::~RefMemoryPool() { }

void* RefMemoryPool::allocate(std::size_t n_bytes) {
    return malloc(n_bytes);
}

void RefMemoryPool::release(void* addr) {
    free(addr);
}

}
}