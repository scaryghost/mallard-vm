#include "mallard/vm/memory_pool.hpp"

namespace mallard {
namespace vm {

struct RefMemoryPool : public MemoryPool {
    virtual ~RefMemoryPool();

    virtual void* allocate(std::size_t n_bytes);
    virtual void release(void* addr);
};

}
}